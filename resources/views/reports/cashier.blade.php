@extends('layouts.app')

@section('content')
    <style>
        #thegrid {
            border-collapse: collapse;
            width: 100%;
        }

        #thegrid > thead > tr > th, #thegrid > tbody > tr > td {
            border: 1px solid black;
        }
    </style>
    <div class="panel panel-default">
        <div class="panel-heading" align="center">
            <b>TAHMEED COURIER COURIER SALES REPORTS</b>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" align="center">
            SYSTEM USER WAYBILL REPORTS
        </div>

        <div class="panel-body">
            <div class="non-print search-panel ">
                <div id="grid-content">
                    @if(count($waybills))
                        <table class="" id="thegrid">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Waybill</th>
                                <th>Date</th>
                                <th>Package</th>
                                <th>From</th>
                                <th>Dest</th>
                                <th>COD - KSH</th>
                                <th>ACC - KSH</th>
                                <th>CASH - KSH</th>
                            </tr>
                            </thead>
                            <tbody id="d-content">
                            @php
                                {{$cod_total = 0.00; $acc_total = 0.00; $cash_total = 0.00;}}
                            @endphp

                            @foreach ($waybills as $waybill)
                                <tr>
                                    <td class="border">{{$loop->index+1}}</td>
                                    <td>{{$waybill->waybill_no}}</td>
                                    <td>{{$waybill->created_at->format('D d-m-Y')}}</td>
                                    <td>{{$waybill->package_types->package_type}}</td>
                                    <td>{{$waybill->origins->office_name}}</td>
                                    <td>{{$waybill->destinations->office_name}}</td>
                                    @php
                                        {{
                                            $cod = 0.00; $acc = 0.00; $cash= 0.00;
                                            $payment_mode = $waybill->payment_mode_id;
                                            $amount = number_format($waybill->amount);

                                            if($payment_mode == CASH_ON_DELIVERY){
                                                $cod_total += $amount;
                                                echo "<td class='border'>{$waybill->amount}</td><td class='border'>0.00</td><td class='border'>0.00</td>";
                                            }else if(ACCOUNT_PAYMENT){
                                                $acc_total += $amount;
                                                echo "<td class='border'>0.00</td><td class='border'>{$amount}</td><td class='border'>0.00</td>";
                                            }else if($payment_mode == CASH_PAYMENT){
                                                $cach_total += $waybill->amount;
                                                echo "<td class='border'>0.00</td><td class='border'>0.00</td><td class='border'>{$amount}</td>";
                                            }
                                        }}
                                    @endphp
                                </tr>
                            @endforeach
                            <tr>
                                <td class='border'></td>
                                <td class='border' colspan="5"><b>TOTAL</b></td>
                                <td class='border'>{{$cod_total}}</td>
                                <td class='border'>{{$acc_total}}</td>
                                <td class='border'>{{$cash_total}}</td>
                            </tr>
                            </tbody>
                        </table>
                    @else
                        <div align="center">
                            No data found!
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div>
        <a href="{{url('print_cashier_report')}}" class="btn btn-primary">
            <span class="glyphicon glyphicon-print"><span/> PRINTABLE PDF</a>
    </div>

@endsection