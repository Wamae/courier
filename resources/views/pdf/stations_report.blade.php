<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Document</title>
    <style>
         table {
            width: 100%;
            border-collapse: collapse;
        }

        thead > tr > th, .border {
            border: 1px solid black;
        }
    </style>
</head>
<body>
<table style="width: 100%;">
    <thead>

    <tr align="center">
        <td colspan="11">{{strtoupper(config("app.name"))}} SALES REPORTS</td>
    </tr>
    <tr align="center">
        <td colspan="11">STATION-{{strtoupper(Auth::user()->station)}}</td>
    </tr>
    <tr align="center">
        <td colspan="11">USER-{{strtoupper(Auth::user()->name)}}</td>
    </tr>

    <tr>
        <th>COLLECTION</th>
        <th colspan="3">COD IN - KSH</th>
        <th colspan="3">ACCOUNT - KSH</th>
        <th colspan="3">CASH - KSH</th>
        <th>COD + CASH - KSH</th>
    </tr>

    <tr>
        <th></th>
        <th>Amount</th>
        <th>V.A.T</th>
        <th>Total</th>
        <th>Amount</th>
        <th>V.A.T</th>
        <th>Total</th>
        <th>Amount</th>
        <th>V.A.T</th>
        <th>Total</th>
        <th>TOTAL</th>
    </tr>

    </thead>
    <tbody>
    @if(count($station_data) > 0)
    <?php
     $cod_amount_total = 0;
     $cod_vat_total = 0;
     $cod_total = 0;

    $acc_amount_total = 0;
     $acc_vat_total = 0;
     $acc_total = 0;

     $cash_amount_total = 0;
     $cash_vat_total = 0;
     $cash_total = 0;

     $cod_cash_total = 0;
    ?>
        @foreach($station_data as $data)
        <?php
        $cod_amount_total  += $data["cod_amount"];
        $cod_vat_total  += $data["cod_vat"];
        $cod_total  += ($data["cod_amount"] +$data["cod_vat"]);

        $acc_amount_total  += $data["acc_amount"];
        $acc_vat_total  += $data["acc_vat"];
        $acc_total  += ($data["acc_amount"] +$data["acc_vat"]);

        $cash_amount_total  += $data["cash_amount"];
        $cash_vat_total  += $data["cash_vat"];
        $cash_total  += ($data["cash_amount"] +$data["cash_vat"]);

        $cod_cash_total += $data["cod_amount"] + $data["cod_vat"]+$data["acc_amount"] + $data["acc_vat"]+$data["cash_amount"] + $data["cash_vat"];

        ?>
            <tr class="border">
                <td class="border">{{$data["office_name"]}}</td>
                <td class="border">{{number_format($data["cod_amount"],2)}}</td>
                <td class="border">{{number_format($data["cod_vat"],2)}}</td>
                <td class="border">{{number_format($data["cod_amount"] + $data["cod_vat"],2)}}</td>
                <td class="border">{{number_format($data["acc_amount"],2)}}</td>
                <td class="border">{{number_format($data["acc_vat"],2)}}</td>
                <td class="border">{{number_format($data["acc_amount"] + $data["acc_vat"],2)}}</td>
                <td class="border">{{number_format($data["cash_amount"],2)}}</td>
                <td class="border">{{number_format($data["cash_vat"],2)}}</td>
                <td class="border">{{number_format($data["cash_amount"] + $data["cash_vat"],2)}}</td>
                <td class="border">{{number_format($data["cod_amount"] + $data["cod_vat"]+$data["acc_amount"] + $data["acc_vat"]+$data["cash_amount"] + $data["cash_vat"],2)}}</td>
            </tr>
        @endforeach
        <tr class="border">
            <td class="border"><b>Grand Totals: </b></td>
            <td class="border"><b>{{number_format($cod_amount_total,2)}}</b></td>
            <td class="border"><b>{{number_format($cod_vat_total,2)}}</b></td>
            <td class="border"><b>{{number_format($cod_total,2)}}</b></td>
            <td class="border"><b>{{number_format($acc_amount_total,2)}}</b></td>
            <td class="border"><b>{{number_format($acc_vat_total,2)}}</b></td>
            <td class="border"><b>{{number_format($acc_total,2)}}</b></td>
            <td class="border"><b>{{number_format($cash_amount_total,2)}}</b></td>
            <td class="border"><b>{{number_format($cash_vat_total,2)}}</b></td>
            <td class="border"><b>{{number_format($cash_total,2)}}</b></td>
            <td class="border"><b>{{number_format($cod_cash_total,2)}}</b></td>
        </tr>
    @else
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    @endif
    </tbody>
</table>
<br>
<br>
<br>
<table>
    <tfoot>
    <tr>
        <td>Report Station:</td>
        <td>ALL STATIONS</td>
    </tr>
    <tr>
        <td>Report Dates:</td>
        <td>17 January 2018 - 17 January 2018</td>
    </tr>
    <tr>
        <td>Printed By:</td>
        <td>Said Sign: ..............................................</td>
    </tr>
    <tr>
        <td>Checked By:</td>
        <td>........................................................................................</td>
        <td>Sign: ..............................................</td>
    </tr>
    </tfoot>
</table>

</body>
</html>