<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Document</title>
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
        }

        thead > tr > th, .border {
            border: 1px solid black;
        }
    </style>
</head>
<body>
<table>
    <thead>
    <tr align="center">
        <td colspan="8"><b>{{strtoupper(config("app.name"))}} SALES REPORTS</b></td>
    </tr>
    <tr align="center">
        <td colspan="8"><b>STATION-{{strtoupper(Auth::user()->stations->office_name)}}</b></td>
    </tr align="center">
    <tr align="center">
        <td colspan="8"><b>USER-{{strtoupper(Auth::user()->name)}}</b></td>
    </tr>
    <tr>
        <th></th>
        <th>Waybill</th>
        <th>Date</th>
        <th>Package</th>
        <th>From</th>
        <th>Dest</th>
        <th>COD - KSH</th>
        <th>ACC - KSH</th>
        <th>CASH - KSH</th>
    </tr>
    </thead>
    <tbody id="d-content">
    @php
        {{$cod_total = 0.00; $acc_total = 0.00; $cash_total = 0.00;}}
    @endphp

    @foreach ($waybills as $waybill)
        <tr class="border">
            <td class="border">{{$loop->index+1}}</td>
            <td class="border">{{$waybill->waybill_no}}</td>
            <td class="border">{{$waybill->created_at->format('D d-m-Y')}}</td>
            <td class="border">{{$waybill->package_types->package_type}}</td>
            <td class="border">{{$waybill->origins->office_name}}</td>
            <td class="border">{{$waybill->destinations->office_name}}</td>
            @php
                {{
                    $cod = 0.00; $acc = 0.00; $cash= 0.00;
                    $payment_mode = $waybill->payment_mode_id;
                    $amount = number_format($waybill->amount);

                    if($payment_mode == CASH_ON_DELIVERY){
                        $cod_total += $amount;
                        echo "<td class='border'>{$waybill->amount}</td><td class='border'>0.00</td><td class='border'>0.00</td>";
                    }else if(ACCOUNT_PAYMENT){
                        $acc_total += $amount;
                        echo "<td class='border'>0.00</td><td class='border'>{$amount}</td><td class='border'>0.00</td>";
                    }else if($payment_mode == CASH_PAYMENT){
                        $cach_total += $waybill->amount;
                        echo "<td class='border'>0.00</td><td class='border'>0.00</td><td class='border'>{$amount}</td>";
                    }
                }}
            @endphp
        </tr>
    @endforeach
    <tr>
        <td class='border'></td>
        <td class='border' colspan="5"><b>TOTAL</b></td>
        <td class='border'>{{$cod_total}}</td>
        <td class='border'>{{$acc_total}}</td>
        <td class='border'>{{$cash_total}}</td>
    </tr>
    </tbody>
</table>
<table>
    <tr>
        <td>Report Dates:</td>
        <td colspan="7">{{date('d M Y')}} - {{date('d M Y')}}</td>
    </tr>
    <tr>
        <td>Printed By:</td>
        <td colspan="4">{{Auth::user()->name}}</td>
        <td>Sign:</td>
        <td colspan="3">..............................................</td>
    </tr>
    <tr>
        <td>Checked By:</td>
        <td colspan="4"></td>
        <td>Sign:</td>
        <td colspan="3">..............................................</td>
    </tr>
</table>
</body>
</html>