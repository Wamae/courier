<?php

namespace App\Http\Controllers;

use App\Waybill;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CashierReportsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $waybills = Waybill::where("created_by", Auth::user()->id)
            ->whereDate('created_at', date('Y-m-d'))
            ->with(['package_types', 'origins', 'destinations'])->get();

        return view('reports.cashier', ['waybills' => $waybills]);
    }

    /**
     * Print Cashiers' report
     * @param Request $request
     * @return
     */
    public function printCashierReport(Request $request)
    {
        $waybills = Waybill::where("created_by", Auth::user()->id)
            ->whereDate('created_at', date('Y-m-d'))
            ->get();

        $pdf = \App::make('dompdf.wrapper');

        $pdf->loadView('pdf.cashiers_report', compact('waybills'))->setPaper('a4', 'landscape');
        return $pdf->stream(Auth::user()->stations->office_name . '_' . '.pdf');
    }
}
